# Reconnaissance d'espèces d'oiseaux par apprentissage profond

Projet de Deep Learning consistant à reconnaitre 315 espèces d'oiseaux différentes.

- code.ipynb est le nootebook où se situe le code Python du projet. code_html.html est sa version html.
- rapport_Martin_Vey_Bird_Recognition_DL.pdf est le rapport du projet.
- pour télécharger les données, rendez-vous sur le lien suivant : https://drive.google.com/file/d/1tKh33obX5Ktkv3Ebow93eFm3WrKcA-Q6/view?usp=sharing. Téléchargez le fichier .zip, extrayez son contenu et placez les dossier train, val, test, images to test et test_bird dans le même dossier que code.ipynb.
- pour télécharger les modèles sauvegardés, rendez-vous sur le lien suivant : https://drive.google.com/file/d/1-S4v7gIqVb_zWAFJbh9ejwna7NeoTkFG/view?usp=sharing. Téléchargez le fichier .zip, extrayez son contenu et placez les dossiers model_finetuned et model_finetuned_64 dans le même dossier que code.ipynb.
